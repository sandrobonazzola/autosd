# The Automotive Stream Distribuition

This is the git repository for the Automotive Stream Distribuiton configurations. It contains recipes for
building the images, information on how to download them, and
documentation.

For more information, visit:

* [Automotive SIG documentation](https://sigs.centos.org/automotive)
